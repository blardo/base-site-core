<?php
namespace d3;

/**
 * Use this class to connect to 1 or more databases
 * @name $db
 * @version 1.5.54
 * @author Brian Liccardo <brian@d3corp.com>
 * @property PDO $pdo
 */
class db {
	# STATIC VARS
	protected static $connected_to;
	protected $non_reportin_errors = array('MySQL server has gone away');
	
	# GENERAL VARS
	private $site_name	= 'Site Name';
	
	# DB CONNECTION VARS
	protected $db_link			= false;
	protected $db_uname			= false;
	protected $db_pword			= false;
	protected $db_name			= false;
	protected $db_host			= false;
	protected $db_type			= false;
	protected $die_on_fail_connection; // Set to false for alternate connections that the site doesn'te depend on
	
	protected $connected		= false;
	protected $db_cache			= false;
	protected $calls_made		= 0;
	protected $recordQueries	= false;
	protected $queries			= array();
	protected $_error			= false;
	protected $charset			= 'utf8';
	protected $last_sql;
	protected $last_sql_is_pdo	= false;
	
	protected static $emailedErrorsCounter = 0;
	protected static $emailedErrorsLimit = 10;

	# DEBUG MODE
	public $debug_mode = false;
  
	# ERROR REPORTING
	protected $error_print				= false;	# Print sql errors to browser
	protected $error_email				= false;	# Email sql errors to
	protected $error_db					= false;	# Insert error into database
	protected $error_email_to			= "errors@d3corp.com";
	protected $error_email_from			= "errors@d3corp.com";
	protected $error_override_email_to	= "notify@d3corp.com"; # Sends emails that are overridden to this as well
	
	private $from_log_error = false;
	
	private $sqli_keywords = array('concat','union select');
	
	/**
	 *
	 * @var PDO 
	 */
	protected $pdo;
	
	/**
	 *
	 * @var false|array 
	 */
	protected $pdo_last_params;
	
	/**
	 * 
	 * @param string $db_uname
	 * @param string $db_pword
	 * @param string $db_name
	 * @param string $db_host
	 * @param bool $die_on_fail_connection
	 * @param string $db_type
	 */
	public function __construct($db_uname=NULL, $db_pword=NULL, $db_name=NULL, $db_host=NULL, $die_on_fail_connection=true, $db_type=NULL) {
		$this->db_uname	= (!is_null($db_uname))	? $db_uname	: ( (defined('DB_UNAME'))	? DB_UNAME	: false );
		$this->db_pword	= (!is_null($db_pword))	? $db_pword	: ( (defined('DB_PWORD'))	? DB_PWORD	: false );
		$this->db_name	= (!is_null($db_name))	? $db_name	: ( (defined('DB_NAME'))	? DB_NAME	: false );
		$this->db_host	= (!is_null($db_host))	? $db_host	: ( (defined('DB_HOST'))	? DB_HOST	: 'localhost' );
		$this->db_type	= (!is_null($db_type))	? $db_type	: ( (defined('DB_TYPE'))	? DB_TYPE	: 'mysqli' );
		
		$this->die_on_fail_connection = $die_on_fail_connection;

		if (defined('DB_ERROR_PRINT'))	$this->error_print	= DB_ERROR_PRINT;
		if (defined('DB_ERROR_EMAIL'))	$this->error_email	= DB_ERROR_EMAIL;
		if (defined('DB_ERROR_DB'))		$this->error_db		= DB_ERROR_DB;
		
		if (defined('DB_ERROR_EMAIL_TO')) {
			$this->error_email_to = DB_ERROR_EMAIL_TO;
			$this->error_email_from = DB_ERROR_EMAIL_FROM;
		}
		
		if (defined('SITE_NAME')) $this->site_name = SITE_NAME;

		if ($this->debug_mode == true) {
			echo "{$this->db_name} - {$this->db_uname} - {$this->db_pword} - {$this->db_host}<br>";
		}
	 	
		// CONNECT TO DATABASE
		$this->db_connect($this->db_uname, $this->db_pword, $this->db_name, $this->db_host);
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getDBType() {
		return $this->db_type;
	}
	
	/**
	 * 
	 * @param string $type
	 * @param boolean $set
	 * @return void
	 */
	public function setError($type, $set) {
		$var = 'error_'.$type;
		if (!isset($this->$var)) return;
		if (!is_bool($set)) return;
		
		$this->$var = $set;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function isConnected() {
		return $this->connected;
	}
	
	/**
	 * 
	 * @param boolean $setto
	 */
	public function setDBCache($setto) {
		$this->db_cache = ($setto == false) ? false : true;
	}
	
	private function _incCalls() {
		if ($this->recordQueries == true) {
			array_push($this->queries, $this->getLastSql());
		}
		$this->calls_made++;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function getCalls() {
		return $this->calls_made;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getQueriesMade() {
		return $this->queries;
	}
	
	/**
	 * Get the database currently connected to
	 * @return string
	 */
	public function getSelectedDB() {
		return (isset($GLOBALS['DBCONNECTED'])) ? $GLOBALS['DBCONNECTED'] : false;
	}
	
	/**
	 * 
	 * @param string $db_name
	 */
	protected function setSelectedDB($db_name) {
		$GLOBALS['DBCONNECTED'] = $db_name;
	}
	
	/**
	 * 
	 * @param string $db_uname username
	 * @param string $db_pword password
	 * @param string $db_name database name
	 * @param string $db_host host
	 * @return boolean
	 */
	public function db_connect($db_uname, $db_pword, $db_name, $db_host='localhost') {
		$connectedTo = $this->getSelectedDB();
		if ($connectedTo == $db_name && $this->db_link !== false) return;
		$this->setSelectedDB($db_name);
		
		switch ($this->db_type) {
			case 'mysql':
				if (!$this->db_link = @mysql_connect($db_host,$db_uname,$db_pword)) {
					if ($this->die_on_fail_connection == true) {
						$this->errorReport('', 'Connection Failed', true, false);
					}
					return false;
				} else {
					if ($this->charset) mysql_set_charset($this->charset, $this->db_link);
					if (!@mysql_select_db($db_name, $this->db_link)) {
						$this->errorReport('', @mysql_error(), true, false);
						return false;
					} else {
						$this->connected = true;
						return true;
					}
				}
				break;
			case 'mysqli':
				$this->db_link = @new mysqli($db_host,$db_uname,$db_pword, $db_name);
				if ($this->db_link->connect_error) {
					if ($this->die_on_fail_connection == true) {
						$this->errorReport('', 'Connection Failed '.$this->db_link->connect_error, true, false);
					}
					return false;
				} else {
					if ($this->charset) $this->db_link->set_charset( $this->charset );
					if (!@mysqli_select_db($this->db_link, $db_name)) {
						$this->errorReport('', mysqli_error($this->db_link), true, false);
						return false;
					} else {
						$this->connected = true;
						return true;
					}
				}
				break;
			case 'mysqlpdo':
				if ($this->pdoConnect()) {
					$this->connected = true;
				}
				return true;
				break;
			case 'mssql':
				if (!$this->db_link = @mssql_connect($db_host,$db_uname,$db_pword, true)) {
					if ($this->die_on_fail_connection == true) {
						$this->errorReport('', 'Connection Failed', true, false);
					}
					return false;
				} else {
					if (!@mssql_select_db($db_name, $this->db_link)) {
						$this->errorReport('', @mssql_get_last_message(), true, false);
						return false;
					} else {
						$this->connected = true;
						return true;
					}
				}
				break;
			case 'sqlsrv':
				$connectionInfo = array("Database"=>$db_name, "UID" => $db_uname, "PWD" => $db_pword);
				$host = ($db_host == 'localhost') ? '(local)' : $db_host;
				$this->db_link = sqlsrv_connect($host, $connectionInfo);

				if (!$this->db_link) {
					if ($this->die_on_fail_connection == true) {
						$this->errorReport('', 'Connection Failed', true, false);
					}
					return false;
				} else {
					$this->connected = true;
					return true;
				}
				break;
			case 'odbc':
				if (!$this->db_link = @odbc_connect($db_name,$db_uname,$db_pword)) {
					if ($this->die_on_fail_connection == true) {
						$this->errorReport('', 'Connection Failed:'.odbc_errormsg(), true, false);
					}
					return false;
				} else {
					$this->connected = true;
					return true;
				}
				break;
		}
	}
	
	private function validateWhere($whereClause,$sqlQuery) {
		$return = true;
		foreach ($this->sqli_keywords as $key) {
			if (is_numeric(strpos(strtolower($whereClause), $key))) {
				$return = false;
			}
		}
		
		if ($return == false) {
			$this->errorReport($sqlQuery, "SQL INJECTION ATTEMPT: " . $sqlQuery, $die = false, $email = true, $override_email = true);
		}
		
		return $return;
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $lock_type
	 * @return boolean
	 */
	public function lockTable($table, $lock_type='READ') {
		$lock_type = strtoupper($lock_type);
		$lock_types = array('READ', 'READ LOCAL', 'WRITE');
		if (!in_array($lock_type, $lock_types)) return false;
		
		$sql = 'LOCK TABLE '.$this->escape($table).' '.$lock_type;
		
		$sqlResult = $this->_returnResult($sql,NULL,NULL,true);
		
		return true;
	}
	
	/**
	 * unlocks tables
	 */
	public function unLockTables() {
		$sql = 'UNLOCK TABLES';
		
		$sqlResult = $this->_returnResult($sql,NULL,NULL,true);
	}
	
	/**
	 * 
	 * @param string $string
	 * @return string
	 */
	public function escape($string) {
		switch ($this->db_type) {
			case 'mysql':
				return mysql_real_escape_string($string);
				break;
			case 'mysqli':
				return mysqli_real_escape_string($this->db_link, $string);
				break;
			case 'mssql':
				return str_replace("'", "''", $string);
				break;
			case 'mysqlpdo':
				// this removed the outer quotes since its for legacy support
				$quoted = $this->pdo->quote($string);
				return substr($quoted, 1, (strlen($quoted) - 2) );
				break;
			default:
				return addslashes($string);
				break;
		}
	}
	
	/**
	 * get raw result from server along with applicable fetch function as a string
	 * @param string $sql
	 * @return array
	 */
	public function getResult($sql) {
		$sqlResult = null;
		$fetchFunc = null;
		switch ($this->db_type) {
			case 'mysql':
				$queryFunc = 'mysql_query';
				$fetchFunc = 'mysql_fetch_assoc';
				
				if (!$sqlResult = @mysql_query($sql, $this->db_link)) {
					if ($this->from_log_error == false) {
						$this->errorReport($sql, mysql_error($this->db_link));
					}
				}
				break;
			case 'mysqli':
				$queryFunc = 'mysqli_query';
				$fetchFunc = 'mysqli_fetch_assoc';
				
				if (!$sqlResult = @mysqli_query($this->db_link, $sql)) {
					if ($this->from_log_error == false) {
						$this->errorReport($sql, mysqli_error($this->db_link));
					}
				}
				break;
		}
		
		return array($fetchFunc, $sqlResult);
	}
	
	/**
	 * 
	 * @param string $sql
	 * @param string $type
	 * @param string $fetch_type
	 * @param boolean $exec_only
	 * @return array
	 */
	private function _returnResult($sql, $type = 'NULL', $fetch_type = 'index', $exec_only = false) {
		$this->db_connect($this->db_uname, $this->db_pword, $this->db_name, $this->db_host);
		
		if (!$this->from_log_error) {
			$this->last_sql = $sql;
		}

		if ($this->debug_mode == true) echo $sql."<br>";
		
		switch ($this->db_type) {
			case 'mysqlpdo':
				switch ($type) {
					case 'firstitem':
						return $this->pdoGetItem($sql);
						break;
					case 'firstrow':
						return $this->pdoFetch($sql);
						break;
					case 'firstcol':
						return $this->pdoFetchColumn($sql);
						break;
					case 'fullarray':
						return $this->pdoFetchAll($sql);
						break;
					case 'insert':
						return $this->pdoInsert($sql);
						break;
					case 'replace':
						$this->pdoExecute($sql);
						return $this->pdo->lastInsertId();
						break;
					default:
						return $this->pdoExecute($sql);
				
				}
				break;
			case 'mysql':
				$queryFunc = 'mysql_query';
				$errorFunc = 'mysql_error';
				$numRowsFunc = 'mysql_num_rows';
				if ($fetch_type == 'index') {
					$fetchFunc = 'mysql_fetch_array';
				} else {
					$fetchFunc = 'mysql_fetch_assoc';
				}
				break;
			case 'mysqli':
				$queryFunc = 'mysqli_query';
				$errorFunc = 'mysqli_error';
				$numRowsFunc = 'mysqli_num_rows';
				if ($fetch_type == 'index') {
					$fetchFunc = 'mysqli_fetch_array';
				} else {
					$fetchFunc = 'mysqli_fetch_assoc';
				}
				
				// EXECUTE
				if (!$sqlResult = $queryFunc($this->db_link, $sql)) {
					if ($this->from_log_error == false) {
						$this->errorReport($sql, $errorFunc($this->db_link));
					}
				}
				break;
			case 'mssql':
				$queryFunc = 'mssql_query';
				$errorFunc = 'mssql_get_last_message';
				$numRowsFunc = 'mssql_num_rows';
				if ($fetch_type == 'index') {
					$fetchFunc = 'mssql_fetch_array';
				} else {
					$fetchFunc = 'mssql_fetch_assoc';
				}
				break;
			case 'sqlsrv':
				$queryFunc = 'sqlsrv_query';
				$errorFunc = 'sqlsrv_get_last_message';
				$numRowsFunc = 'sqlsrv_num_rows';
				// sqlsrv only has this function and by default return both numeric and assoc keys
				$fetchFunc = 'sqlsrv_fetch_array';
				
				// EXECUTE
				if (!$sqlResult = $queryFunc($this->db_link, $sql, array(), array( "Scrollable" => 'static' ) )) {
					if ($this->from_log_error == false) {
						$this->errorReport($sql, sqlsrv_errors());
					}
				}
				break;
			case 'odbc':
				$queryFunc = 'odbc_exec';
				$errorFunc = 'odbc_errormsg';
				$numRowsFunc = 'odbc_num_rows';
				if ($fetch_type == 'index') {
					$fetchFunc = 'odbc_fetch_array';
				} else {
					$fetchFunc = 'odbc_fetch_array';
				}
				
				// EXECUTE
				if (!$sqlResult=$queryFunc($this->db_link,$sql)) {
					if ($this->from_log_error == false) {
						$this->errorReport($sql, $errorFunc());
					}
				}
				break;
		}
		
		// sqlsrv/odbc are different thatn the other connections
		if (!in_array($this->db_type, array('sqlsrv', 'odbc', 'mysqli'))) {	
			if (!$sqlResult = @$queryFunc($sql, $this->db_link)) {
				if ($this->from_log_error == false) {
					$this->errorReport($sql, $errorFunc());
				}
			}
		}
		
		if ($exec_only == true) {
			return $sqlResult;
		}
		
		if ($sqlResult) {
			if ($type == 'insert') {
				switch ($this->db_type) {
					case 'mysqli':
						return mysqli_insert_id($this->db_link);
						break;
					case 'mysql':
						return mysql_insert_id();
						break;
					case 'mssql':
						if ($row = mssql_fetch_row($sqlResult)) {
							$id = trim($row[0]);
						}
						return $id;
						break;
				}	
			}
			
			switch ($type) {
				case 'firstitem':
					$sqlRow	= $fetchFunc($sqlResult);
					if ($this->db_type == 'mysqli') {
						if (mysqli_more_results($this->db_link)) mysqli_next_result($this->db_link);
						mysqli_free_result($sqlResult);
					}
					return	$sqlRow[0];
					break;
				case 'firstrow':
					$tmparray = $fetchFunc($sqlResult);
					if ($this->db_type == 'mysqli') {
						if (mysqli_more_results($this->db_link)) mysqli_next_result($this->db_link);
						mysqli_free_result($sqlResult);
					}
					return (count($tmparray) > 0) ? $tmparray : false;
					break;
				case 'firstcol':
					$tmparray = array();
					while ($row = $fetchFunc($sqlResult)) {
						$tmparray[] = current($row);
					}
					if ($this->db_type == 'mysqli') {
						if (mysqli_more_results($this->db_link)) mysqli_next_result($this->db_link);
						mysqli_free_result($sqlResult);
					}
					return (count($tmparray) > 0) ? $tmparray : false;
					break;
				case 'fullarray':
					$tmparray = array();
					while ($row = $fetchFunc($sqlResult)) {
						if ($fetch_type == 'keyed') {
							$tmparray[ reset($row) ] = $row;
						} else {
							$tmparray[] = $row;
						}
					}
					if ($this->db_type == 'mysqli') {
						if (mysqli_more_results($this->db_link)) mysqli_next_result($this->db_link);
						mysqli_free_result($sqlResult);
					}
					return (count($tmparray) > 0) ? $tmparray : false;
					break;
				default:
					return $sqlResult;
					break;
			}
		}
		return false;
	}
  
	/**
	 * 
	 * @param string $table
	 * @param string $selectClause
	 * @param string $whereClause
	 * @param string $orderClause
	 * @param string $limit
	 * @return array|false
	 */
	public function get2DArrayNames($table, $selectClause, $whereClause=NULL, $orderClause=NULL, $limit=NULL) {
		$sql = "SELECT ".$this->_prepareSelect($selectClause)." FROM ".$table;
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		
		if ($orderClause) $sql .= " ORDER BY $orderClause";
		if ($limit) $sql .= " LIMIT $limit";
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'fullarray', 'names');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $table
	 * @param mixed $selectClause
	 * @param mixed $whereClause
	 * @param string $orderClause
	 * @param string $limit
	 * @return array
	 */
	public function get2DArrayKeyed($table, $selectClause, $whereClause=NULL, $orderClause=NULL, $limit=NULL) {
		$sql = "SELECT ".$this->_prepareSelect($selectClause)." FROM ".$table;
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		if ($orderClause) $sql .= " ORDER BY $orderClause";
		if ($limit) $sql .= " LIMIT $limit";
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'fullarray', 'keyed');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $sql
	 * @return array|false
	 */
	public function get2DArraySQL($sql) {
		return $this->_returnResult($sql, 'fullarray', 'names');
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $selectClause
	 * @param string $whereClause
	 * @param string $orderClause
	 * @param string $limit
	 * @return array|false
	 */
	public function get2DArray($table, $selectClause, $whereClause=NULL, $orderClause=NULL, $limit=NULL) {
		$sql = "SELECT ".$this->_prepareSelect($selectClause)." FROM ".$table;
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		
		if ($orderClause) $sql .= " ORDER BY $orderClause";
		if ($limit) $sql .= " LIMIT $limit";
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'fullarray', 'index');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $selectClause
	 * @param string $whereClause
	 * @param string $orderClause
	 * @param string $limit
	 * @return array|false
	 */
	public function get1DArray($table, $selectClause, $whereClause=NULL, $orderClause=NULL, $limit=NULL) {
		$sql = "SELECT ".$this->_prepareSelect($selectClause)." FROM ".$table;
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		if ($orderClause) $sql .= " ORDER BY $orderClause";
		if ($limit) $sql .= " LIMIT $limit";
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstcol', 'index');
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param string $table
	 * @param string $selectClause
	 * @param string $whereClause
	 * @param string $orderClause
	 * @param string $limit
	 * @return array|false
	 */
	public function getNamedArray($table, $selectClause, $whereClause=NULL, $orderClause=NULL, $limit=1) {
		$sql = "SELECT ".$this->_prepareSelect($selectClause)." FROM ".$table;
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		if ($orderClause) $sql .= " ORDER BY $orderClause";
		if ($limit) $sql .= " LIMIT $limit";
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstrow', 'names');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $sql
	 * @return array|false
	 */
	public function getNamedArraySql($sql) {
		return $this->_returnResult($sql, 'firstrow', 'names');
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $whereFieldName
	 * @param string $whereClause
	 * @return int
	 */
	public function countItems($fromTable, $whereFieldName, $whereClause=NULL) {
		$sql = "SELECT COUNT($whereFieldName) AS counted FROM $fromTable";
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}

		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstitem', 'index');
		} else {
			return 0;
		}
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $whereFieldName
	 * @param string $whereClause
	 * @return number
	 */
	public function sumItems($fromTable, $whereFieldName, $whereClause=NULL) {
		$sql = "SELECT SUM($whereFieldName) AS maxed FROM $fromTable";
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
				
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstitem', 'index');
		} else {
			return 0;
		}
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $whereFieldName
	 * @param string $whereClause
	 * @return string|false
	 */
	public function maxItems($fromTable, $whereFieldName, $whereClause=NULL) {
		$sql = "SELECT MAX($whereFieldName) AS maxed FROM $fromTable";
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstitem', 'index');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $whereFieldName
	 * @param string $whereClause
	 * @return number|false
	 */
	public function avgItems($fromTable, $whereFieldName, $whereClause=NULL) {
		$sql = "SELECT AVG($whereFieldName) AS maxed FROM $fromTable";
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstitem', 'index');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $getField
	 * @param string $whereClause
	 * @return string|false
	 */
	public function getItem($fromTable, $getField, $whereClause=NULL) {
		$sql = "SELECT $getField FROM $fromTable";
		if ($whereClause && $whereClause <> 'all') {
			$whereClause = $this->prepareWhere($whereClause);
			$sql .= " WHERE ".$whereClause;
		}
		
		if ($this->validateWhere($whereClause,$sql)) {
			return $this->_returnResult($sql, 'firstitem', 'index');
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param type $search
	 * @return array|boolean
	 */
	public function getTables($search = NULL) {
		switch ($this->db_type) {
			case 'mysqli':
			case 'mysql':
			case 'mysqlpdo':
				if ($search)
					$sql = "SHOW TABLES LIKE '$search'";
				else
					$sql = "SHOW TABLES";
				
				return $this->_returnResult($sql, 'firstcol', 'index');
				break;
			case 'mssql':
				if ($search)
					$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = '$search'";
				else
					$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'";

				return $this->_returnResult($sql, 'fullarray', 'index');
				break;
		}
		return false;
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $type
	 * @return array|boolean
	 */
	public function getTableColumns($table, $type = '1D') {
		$this->db_connect($this->db_uname, $this->db_pword, $this->db_name, $this->db_host);
	
		switch ($this->db_type) {
			case 'mysqlpdo':
				if ($type == '1D') {
					return $this->pdoFetchColumn("SHOW COLUMNS FROM $table");
				} else {
					return $this->pdoFetchAll("SHOW COLUMNS FROM $table");
				}
				break;
			case 'mysqli':
				$sqlQuery = "SHOW COLUMNS FROM $table";
				
				$this->last_sql = $sqlQuery;  
				
				if ($this->debug_mode == true) echo $sqlQuery."<br>";
	  
				$sqlResult = mysqli_query($this->db_link, $sqlQuery) or $this->errorReport($sqlQuery, mysql_error());
				$numResults = 0;
				if ($sqlResult) {
					$numResults = mysqli_num_rows($sqlResult);
				}
	
				if ($numResults > 0) {
					$inc = 0;
					while ($row = mysqli_fetch_assoc($sqlResult)) {
						if ($type == '1D')
							$tmparray[$inc] = $row['Field'];
						else
							$tmparray[$inc] = $row;
						$inc++;
					}
				} else {
					$tmparray = false;
				}
			break;
			case 'mysql':
				$sqlQuery = "SHOW COLUMNS FROM $table";
				
				$this->last_sql = $sqlQuery;
				
				if ($this->debug_mode == true) echo $sqlQuery."<br>";
	  
				$sqlResult = mysql_query($sqlQuery, $this->db_link) or $this->errorReport($sqlQuery, mysql_error());
				$numResults = mysql_num_rows($sqlResult);
	
				if ($numResults > 0) {
					$inc = 0;
					while ($row = mysql_fetch_assoc($sqlResult)) {
						if ($type == '1D')
							$tmparray[$inc] = $row['Field'];
						else
							$tmparray[$inc] = $row;
						$inc++;
					}
				} else {
					$tmparray = false;
				}
			break;
		case 'mssql':
			$sqlQuery = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE (TABLE_NAME = '$table') AND (TABLE_CATALOG = '".$this->db_name."')";
			
			$this->last_sql = $sqlQuery;
			
			if ($this->debug_mode == true) echo $sqlQuery."<br>";
		  
			$sqlResult = mssql_query($sqlQuery, $this->db_link) or $this->errorReport($sqlQuery, mssql_get_last_message());
			$numResults = mssql_num_rows($sqlResult);

			if ($numResults > 0) {
				$inc = 0;
				while ($row = mysql_fetch_assoc($sqlResult)) {
					if ($type == '1D')
						$tmparray[$inc] = $row['COLUMN_NAME'];
					else
						$tmparray[$inc] = $row;
					$inc++;
				}
			} else {
				$tmparray = false;
			}
			break;
		}
		return $tmparray;
	}
    
	/**
	 * 
	 * @param string $table
	 * @return boolean
	 */
	public function tableExists($table) {
		switch ($this->db_type) {
			case 'mysqlpdo':
				return $this->pdoGetItem("SHOW TABLE STATUS LIKE :table", array(
					':table'	=> $table,
				));
				break;
			case 'mysqli':
				$sql = "SHOW TABLE STATUS LIKE '$table'";
				$sqlResult = $this->_returnResult($sql);
				return (@mysqli_num_rows($sqlResult) == 1);
				break;
			case 'mysql':
				$sql = "SHOW TABLE STATUS LIKE '$table'";
				$sqlResult = $this->_returnResult($sql);
				return (@mysql_num_rows($sqlResult) == 1);
				break;
			case 'mssql':
				$sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='".$table."'";				
				$sqlResult = $this->_returnResult($sql);
				return (@mssql_num_rows($sqlResult) == 1);
				break;				
		}		
		return false;
	}
	
	/**
	 * 
	 * @param string $sql
	 * @return mixed
	 */
	public function exeSQL($sql) {
		return $this->_returnResult($sql,NULL,NULL,true);
	}
	
	/**
	 * 
	 * @param string $sql
	 * @param string $pattern
	 * @return boolean
	 */
	public function exeMultiSQL($sql, $pattern=";") {
		$sql_queries = explode($pattern, $sql);
		
		if (is_array($sql_queries)) {
			for ($i=0; $i < count($sql_queries); $i++) {
				if (trim($sql_queries[$i])) {
					$sqlResult = $this->_returnResult($sql_queries[$i],NULL,NULL,true);
				}
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param string $sql
	 */
	public function update($sql) {
		$sqlResult = $this->_returnResult($sql,NULL,NULL,true);
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $setClause
	 * @param string $whereClause
	 * @return boolean
	 */
	public function updateItem($fromTable, $setClause, $whereClause=false) {
		if (is_array($setClause)) $setClause = $this->convertArray($setClause, 'update');
		
		$sqlQuery = "UPDATE ".$fromTable." SET ".$setClause;
		
		if ($whereClause != 'all' && $whereClause != false) {
			if (is_array($whereClause)) $whereClause = $this->convertArray($whereClause, 'where');
			$sqlQuery .= " WHERE ".$whereClause;
		}
		
		if ($this->validateWhere($whereClause,$sqlQuery)) {
			return $this->update($sqlQuery);
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $data
	 * @param string $where
	 * @return boolean
	 */
	public function updateArray($table, $data, $where=false) {
		$set = $this->convertArray($data, 'update');
		$sqlQuery = "UPDATE $table SET $set";
		
		if ($where) {
			if (is_array($where)) {
				$sqlQuery .= " WHERE ". $this->convertArray($where, 'where');
			} else {
				$sqlQuery .= " WHERE ".$where;
			}
		}
		
		if ($this->validateWhere($where,$sqlQuery)) {
			return $this->update($sqlQuery);
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $sql
	 * @return int
	 */
	public function replace($sql) {
		$this->db_connect($this->db_uname, $this->db_pword, $this->db_name, $this->db_host);
		$this->last_sql = $sql;
		
		return $this->insert($sql);
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $fields
	 * @param string $values
	 * @return mixed
	 */
	public function replaceItem($table, $fields, $values) {
		# REPLACE IS NOT COMPATIBLE WITH MS SQL 2000  
		$sql = "REPLACE INTO $table ($fields) VALUES ($values)";
		return $this->replace($sql);
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $data
	 * @return mixed
	 */
	public function replaceArray($table, $data) {
		list($fields, $values) = $this->convertArray($data, 'insert');
		$sql = "REPLACE INTO $table ($fields) VALUES ($values)";
		return $this->replace($sql);
	}
	
	/**
	 * 
	 * @param string $sql
	 * @return int
	 */
	public function insert($sql) {
		if ($this->db_type == 'mssql') {
			$sql = $sql . "; SELECT @@identity AS id";
		}
		return $this->_returnResult($sql, 'insert');
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $fields
	 * @param string $values
	 * @return int
	 */
	public function insertItem($table, $fields, $values) {
		$sql = "INSERT INTO $table ($fields) VALUES ($values)";
		return $this->insert($sql);
	}
	
	/**
	 * 
	 * @param string $table
	 * @param string $data
	 * @return int
	 */
	public function insertArray($table, $data) {
		list($fields, $values) = $this->convertArray($data, 'insert');
		$sql = "INSERT INTO $table ($fields) VALUES ($values)";
		return $this->insert($sql);
	}
	
	/**
	 * 
	 * @param string $fromTable
	 * @param string $whereClause
	 * @return boolean
	 */
	public function deleteItems($fromTable, $whereClause=false) {
		$sql = 'DELETE FROM `'.$fromTable.'`';
		if ($whereClause !== '') {
			if (is_array($whereClause)) $whereClause = $this->convertArray($whereClause, 'where');
			$sql .= ' WHERE '.$whereClause;
		}
		
		if ($this->validateWhere($whereClause,$sql)) {
			$sqlResult = $this->_returnResult($sql,NULL,NULL,true);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param string $table
	 * @return boolean
	 */
	public function deleteTableData($table) {
		$this->db_connect($this->db_uname, $this->db_pword, $this->db_name, $this->db_host);

		$sql = "TRUNCATE TABLE $table";
		$this->last_sql = $sql;
		
		if ($this->debug_mode == true) echo $sql."<br>";

		switch ($this->db_type) {
			case 'mysqlpdo':
				$this->pdoExecute($sql);
				break;
			case 'mysqli':
				$sqlResult = mysqli_query($this->db_link, $sql) or $this->errorReport($sql, mysqli_error($this->db_link));
				break;
			case 'mysql':
				$sqlResult = mysql_query($sql, $this->db_link) or $this->errorReport($sql, mysql_error());
				break;
			case 'mssql':
				$sqlResult = mssql_query($sql, $this->db_link) or $this->errorReport($sql, mssql_get_last_message());

				$sqlResult = mssql_query("DBCC CHECKIDENT ($table, RESEED, 1)", $this->db_link) or $this->errorReport($sql, mssql_get_last_message());
				break;
		}
		return true;
	}
  
	/**
	 * Gets the last sql string
	 * @return string
	 */
	public function getLastSql() {
		if (substr($this->db_type, -3) == 'pdo' || $this->last_sql_is_pdo == true) {
			return $this->pdoGetLastSql();
		} else {
			return $this->last_sql;
		}
	}
	
	/**
	 * Gets the last pdo sql and puts in the values.
	 * For debug purposes only
	 * 
	 * @return string
	 */
	public function pdoGetLastSql() {
		$sql = $this->last_sql;
		if ($this->pdo_last_params) {
			foreach ($this->pdo_last_params as $k=>$v) {
				$sql = str_replace($k, "'".$this->escape($v)."'", $sql);
			}
		}
		
		return $sql;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function errored() {
		return $this->_error;
	}
	
	/**
	 * 
	 * @param string $sql
	 * @param string $error
	 * @param boolean $die
	 * @param boolean $email
	 * @param boolean $override_email
	 * @return mixed
	 */
	protected function errorReport($sql, $error, $die = false, $email = true, $override_email = false) {
		$this->_error = true;
		
		if (in_array($error, $this->non_reportin_errors)) return;
		
		$referer = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : 'none';
		
		# PRINT
		if ($this->error_print == true) {
			if (isset($_SERVER['SERVER_SOFTWARE'])) {
				$error_table = "
				<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border:2px; border-color:#000000; background-color:#FFFFFF\">
					<tr>
						<td valign=\"top\"><strong>SQL</strong></td>
						<td>".displayHTML($sql)."</td>
					</tr>
					<tr bgcolor=\"#EEEEEE\">
						<td valign=\"top\"><strong>Error</strong></td>
						<td>$error</td>
					</tr>
					<tr>
						<td valign=\"top\"><strong>Date</strong></td>
						<td>".date('Y/m/d H:i:s')."</td>
					</tr>
					<tr>
						<td valign=\"top\"><strong>Trace</strong></td>
						<td>".getDebugBackTrace(true)."</td>
					</tr>
				</table>
				";
			} else {
				$error_table = "\nSQL:$sql\nERROR:$error\nDatanase:{$this->db_name}";
			}
			echo $error_table;
		}
	
		# EMAIL
		if (($this->error_email == true && $email == true) || $override_email == true) {
			if (db::$emailedErrorsCounter <= db::$emailedErrorsLimit) {
				db::$emailedErrorsCounter++;
				$error_email = "
					<div align=\"center\">
					<h3>An SQL Error has occured</h3>
					</div>
					<br>
					<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border:2px; border-color:000; background-color:FFF\" align=\"center\">
					<tr>
					  <td valign=\"top\"><strong>SQL</strong></td>
					  <td>".displayHTML($sql)."</td>
					</tr>
					<tr bgcolor=\"#EEEEEE\">
					  <td valign=\"top\"><strong>Error</strong></td>
					  <td>$error</td>
					</tr>
					<tr>
					  <td valign=\"top\"><strong>Date</strong></td>
					  <td>".date('Y/m/d H:i:s')."</td>
					</tr>
					<tr bgcolor=\"#EEEEEE\">
					  <td valign=\"top\"><strong>URL</strong></td>
					  <td><a href=\"".$this->getCurrentURL()."\">".$this->getCurrentURL()."</a></td>
					</tr>
					<tr>
					  <td valign=\"top\"><strong>Remote User</strong></td>
					  <td>IP/Hostname/Agent<br>".((isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '')."<br>".((isset($_SERVER['REMOTE_ADDR'])) ? @gethostbyaddr($_SERVER['REMOTE_ADDR']) : '')."<br>".((isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '')."</td>
					</tr>
					<tr bgcolor=\"#EEEEEE\">
					  <td valign=\"top\"><strong>File</strong></td>
					  <td>".$_SERVER['SCRIPT_FILENAME']."</td>
					</tr>
					<tr>
					  <td valign=\"top\"><strong>Referer</strong></td>
					  <td>".$referer."</td>
					</tr>
					<tr bgcolor=\"#EEEEEE\">
					  <td valign=\"top\"><strong>Post Data</strong></td>
					  <td>".$this->print_r_html($_POST)."</td>
					</tr>
					<tr>
					  <td valign=\"top\"><strong>Back Trace</strong></td>
					  <td>".getDebugBackTrace(true)."</td>
					</tr>
					</table>
				  ";
				if ($override_email == true) {
					send_email(
						$this->error_email_from, 'D3 Error',
						$this->error_override_email_to, 'D3 Error',
						$error_email,
						$this->site_name." SQL Error",
						true
					);
				}
				send_email(
					$this->error_email_from, 'D3 Error',
					$this->error_email_to, 'D3 Error',
					$error_email,
					$this->site_name." SQL Error",
					$is_html = true
				);
			}
		}

		if ($die == true) {
			if (defined('DOC_ROOT') && file_exists(DOC_ROOT.SUB_FOLDER.'/app/design/templates/default/_error.php')) {
				include(DOC_ROOT.SUB_FOLDER.'/app/design/templates/default/_error.php');
			} else {
				echo 'An error has occured';
			}
			
			die();
		}
	
		# INSERT INTO DB date('Y-m-d H:i:s')
		$this->from_log_error = true;
		if ($this->error_db == true && $this->getTables('log_sql_errors')) {
			$this->pdoInsertData('log_sql_errors', array(
				'query'			=> $sql,
				'sql_error'		=> $error, 
				'error_date'	=> date('Y-m-d H:i:s'), 
				'page'			=> $this->getCurrentURL(), 
				'file_location'	=> $_SERVER['SCRIPT_FILENAME'], 
				'remote_ip'		=> ( ($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''), 
				'remote_host'	=> ( ($_SERVER['REMOTE_ADDR']) ? @gethostbyaddr($_SERVER['REMOTE_ADDR']) : ''), 
				'remote_agent'	=> ( ($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ''), 
				'referer'		=> $referer, 
				'post_data'		=> $this->print_r_html($_POST), 
				'back_trace'	=> getDebugBackTrace(false)
			));
		}
		$this->from_log_error = false;
	}
	
	/**
	 * 
	 * @param boolean $short
	 * @return string
	 */
	protected function getCurrentURL($short = false) {
		if (php_sapi_name() == 'cli') { 
			return 'Command Line';
		} else {
			if ($short == false) {
				if (isset($_SERVER["HTTPS"])) {
					if ($_SERVER["HTTPS"] == 'on') {
						$url = 'https://';
					} else {
						$url = 'http://';
					}
				} else {
					if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') {
						$url = 'https://';
					} else {
						$url = 'http://';
					}
				}

				$url .= $_SERVER['HTTP_HOST'];
			}

			if (isset($GLOBALS['SITE_GLOBALS']['full_url_qs'])) {
				$url .= $GLOBALS['SITE_GLOBALS']['full_url_qs'];
			} else {	
				$url .= $_SERVER['REQUEST_URI'];
			}

			return $url;
		}
	}
	
	protected function print_r_html($array) {
		if (is_array($array)) {
			if (count($array) > 0) {
				return str_replace('<br&nbsp;/>', '<br />', str_replace(' ', '&nbsp;', displayHTML(print_r($array,true))));
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param array $array
	 * @param string $return_type
	 * @return string
	 */
	public function convertArray($array, $return_type) {
		switch ($return_type) {
			case 'insert':
				$fields = '';
				$values = '';
				foreach ($array as $field=>$value) {
					if ($fields != '') {
						$fields .= ',';
						$values .= ',';
					}
					$fields .= "`".$this->escape($field)."`";
					$values .=  ((string)$value == '') ? 'NULL' : "'".$this->escape($value)."'";
				}
				return array($fields, $values);
				break;
			case 'update':
				$update = '';
				foreach ($array as $field=>$value) {
					if ($update != '') {
						$update .= ',';
					}
					$val =  ((string)$value == '') ? 'NULL' : "'".$this->escape($value)."'";
					$update .= "`".$this->escape($field)."`=".$val;
				}
				return $update;
				break;
			case 'where':
				$update = '';
				foreach ($array as $field=>$value) {
					if ($update != '') {
						$update .= ' AND ';
					}
					$val =  ((string)$value == '') ? 'NULL' : "'".$this->escape($value)."'";
					$update .= "`".$this->escape($field)."`=".$val;
				}
				return $update;
				break;
		}
	}
	
	/**
	 * Convert an array to a comma seperated string for use in an IN statement
	 * @param array $arr
	 */
	public function array2In($array, $quote=true) {
		$in = '';
		foreach ($array as $row) {
			if ($in != '') {
				$in .= ',';
			}
			
			if ($quote) {
				$in .= "'".$this->escape($row)."'";
			} else {
				$in .= $this->escape($row);
			}
		}
		return $in;
	}
	
	/**
	 * 
	 * @param array $select
	 * @return string
	 */
	protected function _prepareSelect($select) {
		if (is_array($select)) {
			$select_clause = '';
			
			foreach ($select as $field) {
				if ($select_clause !== '') $select_clause .= ',';
				$select_clause .= $this->prepareField($field);
			}
			
			return $select_clause;
		} else {
			return $select;
		}
	}
	
	/**
	 * 
	 * @param array $where
	 * @return string
	 */
	public function prepareWhere($where) {
		if (is_array($where)) {
			$where_clause = '';
			
			foreach ($where as $row) {
				if (is_array($row)) {
					//0=field, 1=operand 2=value
					switch ($row[1]) {
						case 'isnull':
							$where_clause .= $this->prepareField($row[0]).' IS NULL';
							break;
						case 'notisnull':
							$where_clause .= 'NOT ' . $this->prepareField($row[0]).' IS NULL';
							break;
						default:
							$where_clause .= $this->prepareField($row[0]).' '.$row[1].' '.$this->prepareValue($row[2]);
					}
				} else {
					// AND/OR TODO add in check
					$where_clause .= ' '.$row.' ';
				}
			}
			
			return $where_clause;
		} else {
			return $where;
		}
	}
	
	/**
	 * 
	 * @param string $field
	 * @return string
	 */
	public function prepareField($field) {
		$return_field = '';
		
		$parts = explode('.', $field);
		foreach ($parts as $p=>$part) {
			if ($p > 0) $return_field .= '.';
			$return_field .= '`'.$part.'`';
		}
		
		return $return_field;
	}
	
	/**
	 * 
	 * @param type $value
	 * @return type
	 */
	public function prepareValue($value) {
		return ((string)$value == '') ? 'NULL' : "'".$this->escape($value)."'";
	}
	
	/**
	 * Connect to PDO
	 */
	protected function pdoConnect() {
		if (!$this->pdo) {
			try {
				if (version_compare(PHP_VERSION, '5.3.6') < 0) {
					$this->pdo = new PDO('mysql:host='.$this->db_host.';dbname='.$this->db_name, $this->db_uname, $this->db_pword);
					$this->pdo->exec('set names '.$this->charset);
				} else {
					$this->pdo = new PDO('mysql:host='.$this->db_host.';dbname='.$this->db_name.';charset='.$this->charset, $this->db_uname, $this->db_pword);
				}
			} catch (PDOException $e) {
				if ($this->die_on_fail_connection == true) {
					$this->errorReport('', 'Connection Failed [Host:'.$this->db_host.'] [Database:'.$this->db_name.'] [User:'.$this->db_uname.'] '.$e->getMessage(), true, false);
				}
				return false;
			}

			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
		}
		
		return true;
	}
	
	/**
	 * Returns a 2D Array with field names
	 * @param string $sql
	 * @param false|array $params
	 * @return false|array
	 */
	public function pdoFetchAll($sql, $params=false, $return_as_class = false) {
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// connect
		$this->pdoConnect();
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		$return_as = ($return_as_class === false) ? PDO::FETCH_ASSOC : PDO::FETCH_OBJ;
		return $sth->fetchAll($return_as);
	}
	
	/**
	 * Returns a single row with names
	 * @param string $sql
	 * @param false|array $params
	 * @return false|array
	 */
	public function pdoFetch($sql, $params=false, $return_as_class = false) {
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// connect
		$this->pdoConnect();
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		$return_as = ($return_as_class === false) ? PDO::FETCH_ASSOC : PDO::FETCH_OBJ;
		return $sth->fetch($return_as);
	}
	
	/**
	 * Returns a 1D array
	 * @param string $sql
	 * @param false|array $params
	 * @return false|array
	 */
	public function pdoFetchColumn($sql, $params=false) {
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// connect
		$this->pdoConnect();
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		$data = $sth->fetchAll(PDO::FETCH_NUM);
		if ($data) {
			$return = array();
			foreach ($data as $row) {
				$return[] = $row[0];
			}
			return $return;
		}
		
		return false;
	}
	
	/**
	 * Returns a single item
	 * @param string $sql
	 * @param false|array $params
	 * @return false|array
	 */
	public function pdoGetItem($sql, $params=false) {
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// connect
		$this->pdoConnect();
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		return $sth->fetchColumn();
	}
	
	/**
	 * Returns the id of last insert
	 * @param string $sql
	 * @param false|array $params
	 * @return false|int
	 */
	public function pdoInsert($sql, $params=false) {
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// connect
		$this->pdoConnect();
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		return $this->pdo->lastInsertId();
	}
	
	/**
	 * Returns the id of last insert
	 * @param string $table
	 * @param array $data
	 * @return false|int
	 */
	public function pdoInsertData($table, $data) {
		// connect
		$this->pdoConnect();
		
		// created fields/values
		$params = array();
		$fields = '';
		$values = '';
		foreach ($data as $key=>$val) {
			if ($fields != '') {
				$fields .= ',';
				$values .= ',';
			}
			$fields .= $this->prepareField($key);
			$values .= ':'.$key;
			$params[':'.$key] = $val;
		}
		
		$sql = "INSERT INTO `$table` ($fields) VALUES ($values)";
		
		// set last sql
		if (!$this->from_log_error) {
			$this->last_sql = $sql;
			$this->pdo_last_params = $params;
			$this->last_sql_is_pdo = true;
		}
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		return $this->pdo->lastInsertId();
	}
	
	/**
	 * Returns the id of last insert/update
	 * @param string $table
	 * @param array $data
	 * @return false|int
	 */
	public function pdoReplaceData($table, $data) {
		// connect
		$this->pdoConnect();
		
		// created fields/values
		$params = array();
		$fields = '';
		$values = '';
		foreach ($data as $key=>$val) {
			if ($fields != '') {
				$fields .= ',';
				$values .= ',';
			}
			$fields .= $this->prepareField($key);
			$values .= ':'.$key;
			$params[':'.$key] = $val;
		}
		
		$sql = "REPLACE INTO `$table` ($fields) VALUES ($values)";
		
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		return $this->pdo->lastInsertId();
	}
	
	/**
	 * @param string $table
	 * @param array $data
	 * @param array $where
	 * @return false|int
	 */
	public function pdoUpdateData($table, $data, $where=false) {
		// connect
		$this->pdoConnect();
		
		// created fields/values
		$params = array();
		$fieldvals = '';
		foreach ($data as $key=>$val) {
			if ($fieldvals != '') {
				$fieldvals .= ',';
			}
			$fieldvals .= $this->prepareField($key) . ' = :'.$key;
			$params[':'.$key] = $val;
		}
		
		$wheresql = '';
		if ($where) {
			foreach ($where as $wk=>$wv) {
				if ($wheresql != '') $wheresql .= ' AND ';
				$wheresql .= $this->prepareField($wk).' = :w'.$wk;
				$params[':w'.$wk] = $wv;
			}
		}
		
		$sql = "UPDATE `$table` SET ".$fieldvals;
		if ($wheresql) {
			$sql .= " WHERE ".$wheresql;
		}
		
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		return true;
	}
	
	/**
	 * @param string $sql
	 * @param false|array $params
	 * @return boolean
	 */
	public function pdoExecute($sql, $params=false) {
		// set last sql
		$this->last_sql = $sql;
		$this->pdo_last_params = $params;
		$this->last_sql_is_pdo = true;
		
		// connect
		$this->pdoConnect();
		
		// prepare statement
		$sth = $this->pdo->prepare($sql);
		
		// bind params
		$this->pdoBindParams($sth, $params);
		
		// execute
		if (!$sth->execute()) {
			$arr = $sth->errorInfo();
			$this->errorReport($this->getLastSql(), $arr[2]);
			return false;
		}
			
		// return
		return true;
	}
	
	/**
	 * 
	 * @param PDO Statement $sth
	 * @param false|array $params
	 */
	protected function pdoBindParams(&$sth, $params=false) {
		if ($params) {
			foreach ($params as $var=>$val) {
				if (substr($var, 0, 1) != ':') $var = ':'.$var; // add : if not set
				
				if (is_array($val)) {
					$sth->bindValue($var, $val[0], $val[1]);
				} else {
					if ($val === '' || $val === false || $val === null) {
						$sth->bindValue($var, null, PDO::PARAM_INT);
					} else {
						$sth->bindValue($var, $val);
					}
				}
			}
		}
	}
	
	/**
	 * Returns sql for doing an IN($sql)
	 *	and the array of params for binding
	 * 
	 * @param string $key
	 * @param array $array
	 * @return array
	 */
	public function pdoGetArrayParams($key, $array) {
		$parms = array();
		$params = array();
		foreach ($array as $i=>$field) {
			$param = ':'.$key.$i;
			$parms[] = $param;
			$params[':'.$key.$i] = $field;
		}
		return array(implode(',', $parms), $params);
	}
	
	/**
	 * Convert simple k=>v array to a where/param array
	 * 
	 * @param array $array
	 * @return array
	 */
	public function pdoPrepareSimpleWhere($array) {
		$wheresql = '';
		$params = array();
		if ($array) {
			foreach ($array as $wk => $wv) {
				// simple associative array
				if ($wheresql != '') $wheresql .= ' AND ';
				$wheresql .= $this->prepareField($wk).' = :w'.$wk;
				$params[':w'.$wk] = $wv;
			}
		}
		
		return array($wheresql, $params);
	}
	
	/**
	 * Convert full array to where/param array
	 * 
	 * @param array $array
	 * @return array
	 */
	public function pdoPrepareWhere($array) {
		$wheresql = '';
		$params = array();
		
		if ($array) {
			foreach ($array as $row) {
				if (is_array($row)) {
					//0=field, 1=operand 2=value
					switch ($row[1]) {
						case 'isnull':
							$wheresql .= $this->prepareField($row[0]).' IS NULL';
							break;
						case 'notisnull':
						case 'isnotnull':
							$wheresql .= 'NOT ' . $this->prepareField($row[0]).' IS NULL';
							break;
						default:
							$wheresql .= $this->prepareField($row[0]).' '.$row[1].' :w'.$wk;
							$params[':w'.$wk] = $row[2];
					}
				}
			}
		}

		return array($wheresql, $params);
	}
}